
# Simple Makefile to handle packaging and release to pypi

package:
	python3 setup.py sdist
	python3 setup.py bdist_wheel

upload: package
	twine upload dist/*

test-upload: package
	python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

clean:
	rm -rf *.egg-info
	rm -rf dist
	rm -rf build
	rm -rf __pycache__

package-deps:
	python3 -m pip install --user --upgrade setuptools wheel
	python3 -m pip install --user --upgrade twine
